		<aside class="main-sidebar">
		<!-- sidebar-->
		<section class="sidebar position-relative">
			<div class="multinav">
				<div class="multinav-scroll" style="height: 100%;">
					<!-- sidebar menu-->
					<ul class="sidebar-menu" data-widget="tree">
						<li class="header">Menu</li>
							<li class="treeview">
								<a href="#">
									<i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
									<span>Dashboard</span>
									<span class="pull-right-container">
										<i class="fa fa-angle-right pull-right"></i>
								</a>
								<ul class="treeview-menu">
									<li><a href="<?= base_url() ?>request_cell"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Input Cells</a></li>
									<?php if (session()->get('username') == 'admin') { ?>
										<li><a href="<?= base_url('request_cell/') ?>all_cell_detail"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>List Data Cells</a></li>
									<?php } ?>
								</ul>
							</li>
						</li>
					</ul>
				</div>
			</div>
		</section>
		<!-- <div class="sidebar-footer">
		<a href="javascript:void(0)" class="link" data-bs-toggle="tooltip" title="Settings"><span class="icon-Settings-2"></span></a>
		<a href="mailbox.html" class="link" data-bs-toggle="tooltip" title="Email"><span class="icon-Mail"></span></a>
		<a href="javascript:void(0)" class="link" data-bs-toggle="tooltip" title="Logout"><span class="icon-Lock-overturning"><span class="path1"></span><span class="path2"></span></span></a>
	</div> -->
	</aside>
